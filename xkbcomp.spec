Summary:    XKB keymap compiler
Name:       xkbcomp
Version:    1.4.6
Release:    4%{?dist}
License:    MIT
URL:        https://www.x.org
Source0:    https://www.x.org/pub/individual/app/xkbcomp-%{version}.tar.xz

BuildRequires: make, gcc, libxkbfile-devel
BuildRequires: pkgconfig(x11), pkgconfig(xorg-macros) >= 1.8


%description
The xkbcomp keymap compiler converts a description of an XKB keymap
into one of several output formats. The most common use for xkbcomp
is to create a compiled keymap file (.xkm extension) which can be read
directly by XKB-capable X servers or utilities. The keymap compiler can
also produce C header files or XKB source files. The C header files
produced by xkbcomp can be included by X servers or utilities that need
a built-in default keymap. The XKB source files produced by xkbcomp are
fully resolved and can be used to verify that the files which typically
make up an XKB keymap are merged correctly or to create a single file
which contains a complete description of the keymap.


%package devel
Summary:    XKB keymap compiler development package
Requires:   pkgconfig
Requires:   %{name} = %{version}-%{release}



%description devel
This package provides pkgconfig file for developers who use XKB keymap compiler.



%prep
%autosetup



%build
%configure --disable-silent-rules
%make_build



%install
%make_install



%files
%license COPYING
%{_bindir}/xkbcomp
%{_mandir}/man1/xkbcomp.1*



%files devel
%{_libdir}/pkgconfig/xkbcomp.pc



%changelog
* Thu Sep 26 2024 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 1.4.6-4
- Rebuilt for clarifying the packages requirement in BaseOS and AppStream

* Fri Aug 16 2024 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 1.4.6-3
- Rebuilt for loongarch release

* Fri Sep 08 2023 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 1.4.6-2
- Rebuilt for OpenCloudOS Stream 23.09

* Mon Aug 21 2023 Shuo Wang <abushwang@tencent.com> - 1.4.6-1
- update to 1.4.6

* Fri Apr 28 2023 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 1.4.5-3
- Rebuilt for OpenCloudOS Stream 23.05

* Fri Mar 31 2023 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 1.4.5-2
- Rebuilt for OpenCloudOS Stream 23

* Thu Sep 1 2022 cunshunxia <cunshunxia@tencent.com> - 1.4.5-1
- initial build
